﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : Unit
{
    public GameObject TopDef;
    public GameObject FrontDef;
    public float maxAcceleration;   
    bool top = false;
    float speed;
    float move;

    protected override void UpdateCalculations()
    {
        TopDef.SetActive(false);
        FrontDef.SetActive(false);
        //AnimatorStateInfo aInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (Input.GetKey(KeyCode.W))
        {
            top = true;
        }
        else top = false;

        if (Input.GetMouseButton(1))
        {
            if (top)
            {
                animator.SetInteger("defState", 2);
                TopDef.SetActive(true);
            }
            else
            {
                animator.SetInteger("defState", 1);
                FrontDef.SetActive(true);
            }

        }
        else
        {
            animator.SetInteger("defState", 0);

            if (Input.GetMouseButtonDown(0))
            {
                if (top)
                    animator.SetInteger("attackState", 2);
                else
                    animator.SetInteger("attackState", 1);
            }
        }

        if (Input.GetKey(KeyCode.D))
        {
            move = Mathf.Min(move + Time.deltaTime * 0.5f, maxAcceleration);
        }
        else move = Mathf.Max(move - Time.deltaTime * 0.1f, 0);

        if (Input.GetKey(KeyCode.A))
        {
            move = Mathf.Min(move - Time.deltaTime * 0.5f, maxAcceleration);
        }

        speed = walkSpeed + move;
        animator.SetFloat("speed", Mathf.Abs(speed));

        //aInfo.fullPathHash == Animator.StringToHash("Base Layer.walk"))
    }

    private void FixedUpdate()
    {
        if (!isDied)
            transform.Translate(new Vector2(speed, 0) * Time.deltaTime);
    }

    void SkipAttackState()
    {
        animator.SetInteger("attackState", 0);
    }

    public void EndGame()
    {
        totalDeath = true;
    }   
}