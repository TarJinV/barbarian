﻿using UnityEngine;
using System.Collections;

public delegate void AnimateAction (float t);
public delegate void ResultAction();



public class TimerUnit
{
	public float time;
	public float duration;
	public System.Action<float> animation_act = null;
	public System.Action finish_act = null;

	public bool is_active;

	public TimerUnit(float start_time, float action_time, System.Action<float> animation_act, System.Action finish_act)
	{
		this.time = action_time - start_time;
		this.duration = action_time;
		this.animation_act = animation_act;
		this.finish_act = finish_act;
		this.is_active = true;
	}

	public void Stop()
	{
		is_active = false;
		Timer.Remove(this);
	}

	public void Update()
	{
		time -= Time.deltaTime;
		if(time <= 0)
		{
			time = 0;
		}
		if(time <= duration)
		{
			if(animation_act != null)
			{
				animation_act(1f - (time / duration));
			}
		}
		if(time <= 0)
		{
			Stop();
			if (finish_act != null)
			{
				finish_act();
			}
		}
	}

	public void ForceComplete()
	{
		time = 0;
		Update();
	}
}

public class Timer : MonoBehaviour
{
	public static Timer instance = null;
	public int active_cnt = 0;

	TimerUnit[] timers_pool = new TimerUnit[10];



	public static TimerUnit Add(float time, System.Action resultAction)
	{
		return Add(0, time, null, resultAction);
	}

	public static TimerUnit Add(float fullTime, System.Action<float> animateAction, System.Action resultAction = null)
	{
		return Add(0, fullTime, animateAction, resultAction);
	}

	public static TimerUnit Add(float startTime, float fullTime, System.Action<float> animateAction, System.Action resultAction = null)
	{
		if(instance == null)
		{
			Timer timer = GameObject.FindObjectOfType<Timer>();
			if(timer != null)
			{
				instance = timer;
			}
			else
			{
				GameObject timerObject = new GameObject("GlobalTimer");
				DontDestroyOnLoad(timerObject);
				instance = timerObject.AddComponent<Timer>();
			}
		}
		return instance.AddTimer(startTime, fullTime, animateAction, resultAction);
	}

	public static void Stop(ref TimerUnit timer)
	{
		if(timer != null)
		{
			Remove(timer);
			timer = null;
		}
	}

	public static void Remove(TimerUnit timer)
	{
		if(instance != null)
		{
			for(int i = 0; i < instance.timers_pool.Length; i++)
			{
				if(instance.timers_pool[i] == timer)
				{
					instance.timers_pool[i] = null;
				}
			}
		}
	}

    public static void RemoveAll()
    {
        if (instance != null)
        {
            for (int i = 0; i < instance.timers_pool.Length; i++)
            {
                instance.timers_pool[i] = null;
            }
        }
    }

    TimerUnit AddTimer(float startTime, float actionTime, System.Action<float> animAction, System.Action resultAction)
	{
		TimerUnit t_unit = new TimerUnit(startTime, actionTime, animAction, resultAction);

		int unit_idx = -1;
		for(int i = 0; i < timers_pool.Length; i++)
		{
			if(timers_pool[i] == null)
			{
				timers_pool[i] = t_unit;
				unit_idx = i;
				break;
			}
		}
		if(unit_idx < 0)
		{
			int old_count = timers_pool.Length;
			TimerUnit[] new_timers_pool = new TimerUnit[old_count + 5];
			for(int i = 0; i < old_count; i++)
			{
				new_timers_pool[i] = timers_pool[i];
			}
			timers_pool = new_timers_pool;
			timers_pool[old_count] = t_unit;
		}
		return t_unit;
	}

	void Update()
	{
		for(int i = 0; i < timers_pool.Length; i++)
		{
			if(timers_pool[i] != null)
			{
				timers_pool[i].Update();
			}
		}
	}
}
