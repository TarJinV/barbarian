﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathPanel : MonoBehaviour
{
    public float MaxShowTime = 2f;
    public Image image;
    Color color;
    public GameObject ButtonPanel;


    private void Awake()
    {
        color = image.color;
    }

    public void ActivePanel()
    {
        color.a = 0;
        image.color = color;
        ButtonPanel.SetActive(false);

        Timer.Add(MaxShowTime, (animationTime)=> 
        {
            color.a = animationTime;
            image.color = color;
        }, ()=> { ButtonPanel.SetActive(true);});
    }       
}
