﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MainCamera : MonoBehaviour
{
    Vector3 startPosition;

    private void Awake()
    {
        startPosition = transform.position;
    }

    public void ToStartingPosition()
    {
        transform.position = startPosition;
    }

    public void Move(float x)
    {
        transform.position = new Vector3((x + 1) * 0.01f + transform.position.x * 0.99f, transform.position.y, transform.position.z);
    }

    public Vector3 GetCameraPosition()
    {
        return transform.position;
    }
}