﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spaun_blocks : MonoBehaviour
{
    Transform player;
    public Transform[] prefab;
    int count = 0;
    float width = 3.2f;


    public void Init(Transform player)
    {
        this.player = player;
    }

    void Update()
    {
        if (player.position.x > count*width-10)
        {
            count++;
            Instantiate(prefab[Random.Range(0,prefab.Length)], new Vector3(count* width, 0, 0), Quaternion.identity);
        }
	
	}
}
