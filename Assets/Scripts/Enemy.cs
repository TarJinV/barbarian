﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Unit
{
    protected Vector3 playerPos;
    public AttackArea attackArea;
    bool structDamage = false;
    bool score = false;
    public float timeToDestroy = 0;


    public void SetPlayerPosition(Vector3 pos)
    {
        playerPos = pos;
    }

    void OnBecameInvisible()
    {
        if (!isDied)
        {
            structDamage = true;
        }       
    }

    public bool IdentifyScore()
    {
        bool value = false;
        if (score)
        {
            value = true;
            score = false;
        }
        return value;
    }

    public bool IdentifyConstructDamage()
    {
        bool value = false;
        if (structDamage)
        {
            value = true;
            structDamage = false;
        }
        return value;
    }

    protected override void OtherActivitiesInDeathBlow()
    {
        score = true;
    }

    public override void AttackSetActive(bool value)
    {
        attackArea.gameObject.SetActive(value);
    }    

    protected override void DestroyUnit()
    {
        if (!totalDeath)
        {
            timeToDestroy += Time.deltaTime;

            if (timeToDestroy >= 5)
            {
                Destroy(gameObject);
                totalDeath = true;
            }
        }            
    }
}
