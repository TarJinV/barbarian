﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Construction : MonoBehaviour
{
    public GameObject[] sprites;
    int lifeElementsNumber;
    public int LifeElementsNumber
    {
        get { return lifeElementsNumber; }
    }


    private void Awake()
    {
        lifeElementsNumber = sprites.Length - 1;
    }

    public void Move(float x)
    {
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    public bool Destroyed()
    {
        bool value = false;
        sprites[lifeElementsNumber].SetActive(false);
        lifeElementsNumber--;
        
        Debug.Log("defenderNumber" + lifeElementsNumber);
        if (lifeElementsNumber == 0)
        {
            value = true;
        }
        return value;
    }  
}
