﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeArea : MonoBehaviour
{
    public Unit unit;
    public string attackTeg;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == attackTeg)
        {
            unit.GetDeathBlow();           
        }
    }
}
