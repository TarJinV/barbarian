﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCanvas : MonoBehaviour
{
    static GameCanvas instance;
    public GameObject[] Panels;
    public static GameCanvas Instance
    {
        get { return instance; }
    }
    Game game;


    private void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        game = Game.Instance;
        ShowMainMenu();
    }

    public void ActivateWinPanel(int killed, int protect, float time)
    {
        Panels[3].GetComponent<WinPanel>().ActivePanel(killed, protect, time);
        Panels[3].SetActive(true);
    }

    public void ShowMainMenu()
    {
        game.ToStartingPosition();
        HideAllPanell();
        SetActivePanel(0, true);
    }

    public void ClickStartAdventure()
    {
        game.ToStartingPosition();
        game.CreateLvl(0);
        HideAllPanell();
        Panels[2].GetComponent<LevelPanel>().ActivePanel();
        Panels[2].SetActive(true);
    }

    public void SetActivePanel(int number, bool value)
    {
        Panels[number].SetActive(value);
    }

    public void HideAllPanell()
    {
        for (int i = 0; i < Panels.Length; i++)
        {
            Panels[i].SetActive(false);
        }
    }

    public void ActivateDeathPanel()
    {
        Panels[1].GetComponent<DeathPanel>().ActivePanel();
        Panels[1].SetActive(true);
    }

    public void Exit()
    {
        Application.Quit();
    }   
}
