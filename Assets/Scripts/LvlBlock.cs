﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlBlock : MonoBehaviour
{
    public bool obstacle = false; //препятствие.
    public bool unitDeployment = true; //можно ли размещять префабы врагов.

    public bool GetStateOfUnitDeployment()
    {
        if (obstacle || !unitDeployment)
            return false;
        else
            return true;
    }
}
