﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelPanel : MonoBehaviour
{
    public float MaxShowTime = 2f;
    public Image image;
    Color color;


    private void Awake()
    {
        color = image.color;
    }

    public void ActivePanel()
    {
        color.a = 0;
        image.color = color;

        Timer.Add(MaxShowTime, (animationTime) =>
        {
            color.a = 1 - animationTime;
            image.color = color;
        }, () => { gameObject.SetActive(false); });
    }
}
