﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackArea : MonoBehaviour
{
    public Unit unit;
    public string defTeg;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == defTeg)
        {
            unit.AddStunEffect();
        }
    }
}
