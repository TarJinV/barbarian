﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public Animator animator;
    public LifeArea lifeArea;
    public float walkSpeed;
    public int stunTime; //убрать при добавлении анимаций
    int stunEffect; //убрать при добавлении анимаций
    public string animatorDeathTeg;
    public string animatorStunTeg;

    protected bool isDied = false;
    protected bool totalDeath = false;
    protected bool isStuned = false;
    public bool IsDied
    {
        get { return isDied; }
    }
    public bool TotalDeath
    {
        get { return totalDeath; }       
    }


    private void Update()
    {
        if (!isDied)
        {
            if (isStuned)
                DecreaseStunEffect();
            else
                UpdateCalculations();
        }
        else DestroyUnit();
    }

    private void FixedUpdate()
    {
        FixedUpdateCalculations();
    }

    public void SetActiveLifeArea(bool value)
    {
        lifeArea.gameObject.SetActive(value);
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void GetDeathBlow()
    {
        isDied = true;
        animator.SetBool(animatorDeathTeg, true);
        SetActiveLifeArea(false);
        AttackSetActive(false);
        OtherActivitiesInDeathBlow();
    }

    public void AddStunEffect()
    {
        if (!isStuned)
        {
            isStuned = true;
            animator.SetBool(animatorStunTeg, true);
            stunEffect = stunTime;
            AttackSetActive(false);
        }
    }

    void DecreaseStunEffect()
    {
        stunEffect -= 1;
        if (stunEffect == 0)
        {
            isStuned = false;
            animator.SetBool(animatorStunTeg, false);
            AttackSetActive(true);
        }
    }

    virtual protected void OtherActivitiesInDeathBlow()
    {

    }
    virtual public void AttackSetActive(bool value)
    {

    }
    virtual protected void DestroyUnit()
    {

    }
    virtual protected void UpdateCalculations()
    {

    }
    virtual protected void FixedUpdateCalculations()
    {

    }    
}
