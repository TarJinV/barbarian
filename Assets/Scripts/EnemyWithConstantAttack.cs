﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWithConstantAttack : Enemy
{
    protected override void FixedUpdateCalculations()
    {
        if (!isDied && !isStuned)
            transform.Translate(new Vector2(-walkSpeed, 0) * Time.deltaTime);
    }
}
