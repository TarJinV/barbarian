﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinPanel : MonoBehaviour {

    public float MaxShowTime = 2f;
    public Image image;
    Color color;
    public GameObject ButtonPanel;
    public Text killedCount;
    public Text protectedCount;
    public Text timeCount;

    private void Awake()
    {
        color = image.color;
    }

    public void ActivePanel(int killed, int protect, float time)
    {
        color.a = 0;
        image.color = color;
        ButtonPanel.SetActive(false);

        killedCount.text = killed.ToString();
        protectedCount.text = protect.ToString();
        timeCount.text = time.ToString();

        Timer.Add(MaxShowTime, (animationTime) =>
        {
            color.a = animationTime;
            image.color = color;
        }, () => { ButtonPanel.SetActive(true); });
    }
}
