﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    static Game instance;
    LevelGen levelGen;
    UnitGen unitGen;
    MainCamera mainCamera;
    CurentLvl lvl = null;
    public static Game Instance
    {
        get { return instance; }
    }


    private void Update()
    {
        if (lvl != null)
        {
            lvl.Calculations();
        }
    }

    void Start()
    {
        LoadAndInit();
        GameCanvas.Instance.StartGame();
    }

    private void Awake()
    {
        instance = this;
    }

    public void CreateLvl(int constructionNumber)
    {     
        Construction construction = null;
        if (constructionNumber != -1)
            construction = levelGen.CreateConstruction(constructionNumber);

        Player player = unitGen.CreatePlayer();
        GameObject[] levelBioms = levelGen.CreateBioms(30);
        List<Enemy> enemies = unitGen.CreateEnemies(levelBioms);

        lvl = new CurentLvl(player, enemies, levelBioms, mainCamera, construction);
    }

    GameObject LoadPlayer()
    {
        GameObject player = Resources.Load<GameObject>("Prefabs/Player/player");
        if (player == null)
        {
            Debug.Log("LoadPlayer() не нашел префаб игрока");
        }
        return player;
    }
    GameObject[] LoadBioms()
    {
        GameObject[] bioms = Resources.LoadAll<GameObject>("Prefabs/Bioms");
        if (bioms.Length == 0)
        {
            Debug.Log("LoadBioms() не нашел префабы биомов");
        }
        return bioms;
    }
    GameObject[] LoadEnemies()
    {
        GameObject[] enemies = Resources.LoadAll<GameObject>("Prefabs/Enemies");
        if (enemies.Length == 0)
        {
            Debug.Log("LoadEnemies() не нашел префабы врагов");
        }
        return enemies;
    }
    GameObject[] LoadConstructions()
    {
        GameObject[] constructions = Resources.LoadAll<GameObject>("Prefabs/Constructions");
        if (constructions.Length == 0)
        {
            Debug.Log("LoadConstructions() не нашел префабы конструкций");
        }
        return constructions;
    }

    void LoadAndInit()
    {
        mainCamera = gameObject.GetComponent<MainCamera>();
        levelGen = gameObject.GetComponent<LevelGen>();
        unitGen = gameObject.GetComponent<UnitGen>();

        GameReferens referenses = new GameReferens(LoadBioms(), LoadEnemies(), LoadPlayer(), LoadConstructions());

        levelGen.Init(referenses);
        unitGen.Init(referenses);
    }

    public void ToStartingPosition()
    {
        DeliteOldLvl();
        mainCamera.ToStartingPosition();
    }

    void DeliteOldLvl()
    {
        if (lvl != null)
        {
            lvl.DestroyAll();
            lvl = null;
        }          
    }     
}
