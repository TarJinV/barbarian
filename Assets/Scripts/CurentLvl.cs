﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CurentLvl
{
    public CurentLvl(Player player, List<Enemy> enemies, GameObject[] levelBioms, MainCamera mainCamera, Construction construction)
    {
        this.player = player;
        this.enemies = enemies;
        this.levelBioms = levelBioms;
        this.mainCamera = mainCamera;
        this.construction = construction;
    }

    Player player;
    List<Enemy> enemies;
    GameObject[] levelBioms;
    MainCamera mainCamera;
    Construction construction;
    bool constructDestroed = false;
    bool andGame = false;
    int deadEnemy = 0;
    int maxDeadEnemy = 20;
    float time = 0;

    public void Calculations()
    {
        if (!andGame)
        {
            time += Time.deltaTime;
            Vector3 plPos = player.GetPosition();
            mainCamera.Move(plPos.x);

            if (construction != null)
            {
                construction.Move(mainCamera.GetCameraPosition().x);
            }

            CheckEnemies(plPos);

            if (player.TotalDeath || constructDestroed)
                LoseLevel();

            else if (deadEnemy >= maxDeadEnemy)
                WinLevel();
        }
    }

    void WinLevel()
    {
        andGame = true;
        player.SetActiveLifeArea(false);
        GameCanvas.Instance.ActivateWinPanel(deadEnemy, construction.LifeElementsNumber, time);
    }

    void CheckEnemies(Vector3 plPos)
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            Enemy enemy = enemies[i];

            if (enemy.TotalDeath)
            {
                enemies.RemoveAt(i);
                i--;
                continue;
            }
            else
            {
                enemy.SetPlayerPosition(plPos);
                if (enemy.IdentifyScore())
                    deadEnemy += 1;

                if (construction != null && !player.IsDied && !constructDestroed)
                {
                    if (enemy.IdentifyConstructDamage())
                        constructDestroed = construction.Destroyed();
                }
            }       
        }
    }

    void LoseLevel()
    {
        andGame = true;
        player.SetActiveLifeArea(false);
        GameCanvas.Instance.ActivateDeathPanel();
    }

    public void DestroyAll()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] != null)
                MonoBehaviour.Destroy(enemies[i].gameObject);
        }
        enemies.Clear();

        for (int i = 0; i < levelBioms.Length; i++)
        {
            MonoBehaviour.Destroy(levelBioms[i]);
        }

        MonoBehaviour.Destroy(player.gameObject);
        MonoBehaviour.Destroy(construction.gameObject);
    }
}
