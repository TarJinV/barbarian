﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWithHiddenAttack : Enemy
{
    public string animatorAttackTeg;
    public float attackDistance;
    bool inStrike = false;


    public void SetCanGo()
    {
        inStrike = false;
    }

    protected override void UpdateCalculations()
    {
        if (!inStrike && !isDied)
        {
            float attackResult = transform.position.x - playerPos.x;

            if (attackResult <= attackDistance && attackResult > 0)
            {
                animator.SetTrigger(animatorAttackTeg);
                inStrike = true;
            }
        }
    }

    protected override void FixedUpdateCalculations()
    {
        if (!isDied && !isStuned && !inStrike)
            transform.Translate(new Vector2(-walkSpeed, 0) * Time.deltaTime);
    }
}
