﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelGen : MonoBehaviour
{
    GameReferens referenses;

    public void Init(GameReferens referenses)
    {
        this.referenses = referenses;
    }

    public Construction CreateConstruction(int number)
    {
        GameObject construction = MonoBehaviour.Instantiate(referenses.ConstructionsPrefabs[number]);
        return construction.GetComponent<Construction>();
    }

    public GameObject[] CreateBioms(int chunkCount)
    {
        GameObject[] bioms = new GameObject[chunkCount];
        GameObject[] prefabs = referenses.BiomBlocks;

        for (int i = 0; i < chunkCount; i++)
        {
            Vector3 position = new Vector3(referenses.StartPosition.x + (i* referenses.Width), 0, 0);

            if (i < 3)
            {
                bioms[i] = MonoBehaviour.Instantiate(prefabs[0], position, Quaternion.identity);
                bioms[i].GetComponent<LvlBlock>().unitDeployment = false;
            }
            else
            {
                bioms[i] = MonoBehaviour.Instantiate(prefabs[UnityEngine.Random.Range(0, prefabs.Length)], position, Quaternion.identity);
                if(i == 6 || i == chunkCount - 1)
                {
                    bioms[i].GetComponent<LvlBlock>().unitDeployment = false;
                }
            }
        }

        return bioms;
    }
}


