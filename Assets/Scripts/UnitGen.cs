﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitGen : MonoBehaviour
{
    GameReferens referenses;
    List<Enemy> genEnemies;
    float xCorrection;

    public void Init(GameReferens referenses)
    {
        this.referenses = referenses;
    }

    public Player CreatePlayer()
    {
        GameObject player = Instantiate(referenses.PlayerPrefab);
        return player.GetComponent<Player>();
    }

    public List<Enemy> CreateEnemies(GameObject[] lvlBioms)
    {
        genEnemies = new List<Enemy>();

        for (int i = 0; i < lvlBioms.Length; i++)
        {
            xCorrection = referenses.StartPosition.x + (i * referenses.Width);
            if (lvlBioms[i].GetComponent<LvlBlock>().GetStateOfUnitDeployment())
            {
                DeploymentUnit(i);
            }
        }

        return genEnemies;
    }

    void DeploymentUnit(int blockCount)
    {
        for (int i = 0; i < referenses.Positions.Length; i++)
        {
            if (UnityEngine.Random.Range(0, 5) < 2)
                continue;

            GameObject enemy = Instantiate(referenses.EnemiesPrefabs[UnityEngine.Random.Range(0, 3)]);
            enemy.transform.position = new Vector3(xCorrection + referenses.Positions[i], enemy.transform.position.y, enemy.transform.position.z);
            genEnemies.Add(enemy.GetComponent<Enemy>());
        }
    }
}
