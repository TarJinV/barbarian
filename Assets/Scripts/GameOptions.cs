﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameReferens
{
    public GameReferens(GameObject[] biomBlocks, GameObject[] enemiesPrefabs, GameObject playerPrefab, GameObject[] constructions)
    {
        this.biomBlocks = biomBlocks;
        this.enemiesPrefabs = enemiesPrefabs;
        this.playerPrefab = playerPrefab;
        this.constructions = constructions;
    }

    GameObject[] biomBlocks;
    GameObject[] enemiesPrefabs;
    GameObject[] constructions;
    GameObject playerPrefab;
    float width = 3.2f;
    Vector3 startPosition = new Vector3(-3.2f, 0, 0); 
    float[] positions = { -1, -.5f, 0, .5f, 1 };

    public GameObject[] BiomBlocks { get { return biomBlocks; } }
    public GameObject[] EnemiesPrefabs { get { return enemiesPrefabs; } }
    public GameObject[] ConstructionsPrefabs { get { return constructions; } }
    public GameObject PlayerPrefab { get { return playerPrefab; } }
    public float Width { get { return width; } }
    public Vector3 StartPosition { get { return startPosition; } }
    public float[] Positions { get { return positions; } }
}
